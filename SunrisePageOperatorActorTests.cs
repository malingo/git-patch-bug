﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Akka.Actor;
using Akka.TestKit;
using Akka.TestKit.Xunit2;
using FluentAssertions;
using Moq;
using RemoteSigns.Gateway.Client.v1.Models;
using RemoteSigns.Gateway.Client.v1.Models.Metrics;
using Proton.Actors.Displays.Sunrise;
using Proton.Actors.Displays.Sunrise.Messages;
using Proton.Actors.Displays.Sunrise.Messages.Requests;
using Proton.Actors.Displays.Sunrise.Messages.Responses;
using Proton.Actors.Messages;
using Proton.Services;
using Xunit;
using System.Collections;
using AutoFixture;

namespace Proton.UnitTests.Actors.Displays.Sunrise
{
    public class SunrisePageOperatorActorTests : TestKit
    {
        private TestScheduler Scheduler => (TestScheduler) Sys.Scheduler;

        private readonly TestActorRef<SunrisePageOperatorActor> _actor;
        private readonly TestProbe _ttsActor;

        public SunrisePageOperatorActorTests()
            : base(@"akka.scheduler.implementation = ""Akka.TestKit.TestScheduler, Akka.TestKit""")
        {
            _ttsActor = CreateTestProbe();

            var childActorService = new Mock<ChildActorService>();
            childActorService
                .Setup(service => service.GetChild<TtsActor>(It.IsAny<IUntypedActorContext>(), "tts"))
                .Returns(_ttsActor);

            _actor = ActorOfAsTestActorRef<SunrisePageOperatorActor>(
                Props.Create(() => new SunrisePageOperatorActor(childActorService.Object)),
                TestActor);
        }

        [Theory]
        [ClassData(typeof(MetricsOnlyProgramGenerator))]
        public void Should_program_sign_for_metrics_only(
            SignType signType,
            string expectedProgram)
        {
            var state = new SignState
            {
                Metrics = new List<MetricPair>
                {
                    new MetricPair
                    {
                        Key = new ArrivalMetricKey
                        {
                            RouteShortName = "1",
                            AgencyName = "Metro",
                            Headsign = "To 1 somewhere"
                        },
                        Value = new ArrivalMetricValue
                        {
                            Arrivals = new List<int> {1}
                        }
                    },
                    new MetricPair
                    {
                        Key = new ArrivalMetricKey
                        {
                            RouteShortName = "2",
                            AgencyName = "Metro",
                            Headsign = "To 2 somewhere"
                        },
                        Value = new ArrivalMetricValue
                        {
                            Arrivals = new List<int> {2}
                        }
                    },
                    new MetricPair
                    {
                        Key = new ArrivalMetricKey
                        {
                            RouteShortName = "3",
                            AgencyName = "Commerce",
                            Headsign = "To 3 somewhere"
                        },
                        Value = new ArrivalMetricValue
                        {
                            Arrivals = new List<int> {3}
                        }
                    },
                    new MetricPair
                    {
                        Key = new ArrivalMetricKey
                        {
                            RouteShortName = "1234",
                            AgencyName = "Metro",
                            Headsign = "To 4 somewhere"
                        },
                        Value = new ArrivalMetricValue
                        {
                            Arrivals = new List<int> {4}
                        }
                    }
                },
                Messages = new List<SignMessage>
                {

                }
            };

            var config = new SignConfiguration
            {
                SignType = signType
            };
            var protocol = CreateTestProbe();
            _actor.Tell((config, (IActorRef)protocol, state));

            protocol.ExpectMsg<ClearSign>();
            _actor.Tell(new Ack());

            protocol.ExpectMsg<SetRequest>(); // brightness
            _actor.Tell(new Ack());

            protocol.ExpectMsg<DownloadRequest>(request =>
            {
                var program = Encoding.ASCII.GetString(request.BodyData);
                program
                    .Should()
                    .Be(expectedProgram);
            });

            _ttsActor.ExpectMsg<IActorRef>().Should().Be(protocol);
            _ttsActor.ExpectMsg<SignState>().Should().Be(state);
        }

        [Theory]
        [ClassData(typeof(MetricsAndMessageProgramGenerator))]
        public void Should_program_sign_for_metrics_and_messages(
            SignType signType,
            string expectedProgram)
        {
            var state = new SignState
            {
                Metrics = new List<MetricPair>
                {
                    new MetricPair
                    {
                        Key = new ArrivalMetricKey
                        {
                            RouteShortName = "1",
                            AgencyName = "Commerce",
                            Headsign = "To Downtown"
                        },
                        Value = new ArrivalMetricValue
                        {
                            Arrivals = new List<int> {1}
                        }
                    },
                    new MetricPair
                    {
                        Key = new ArrivalMetricKey
                        {
                            RouteShortName = "2",
                            AgencyName = "Metro",
                            Headsign = "To Mall"
                        },
                        Value = new ArrivalMetricValue
                        {
                            Arrivals = new List<int> {2}
                        }
                    },
                },
                Messages = new List<SignMessage>
                {
                    new SignMessage
                    {
                        Id = 12,
                        Message = new Message
                        {
                            Text = "Test Message."
                        }
                    }
                }
            };

            var config = new SignConfiguration
            {
                SignType = signType
            };
            var protocol = CreateTestProbe();
            _actor.Tell((config, (IActorRef)protocol, state));

            protocol.ExpectMsg<ClearSign>();
            _actor.Tell(new Ack());

            protocol.ExpectMsg<SetRequest>(); // brightness
            _actor.Tell(new Ack());

            protocol.ExpectMsg<DownloadRequest>(request =>
            {
                var program = Encoding.ASCII.GetString(request.BodyData);
                program
                    .Should()
                    .Be(expectedProgram);
            });

            _ttsActor.ExpectMsg<IActorRef>().Should().Be(protocol);
            _ttsActor.ExpectMsg<SignState>().Should().Be(state);
        }

        [Theory]
        [ClassData(typeof(LongerMetricsAndMessageProgramGenerator))]
        public void Should_program_sign_for_longer_metrics_and_messages(
            SignType signType,
            string expectedProgram)
        {
            var state = new SignState
            {
                Metrics = new List<MetricPair>
                {
                    new MetricPair
                    {
                        Key = new ArrivalMetricKey
                        {
                            RouteShortName = "1",
                            AgencyName = "Commerce",
                            Headsign = "To Downtown"
                        },
                        Value = new ArrivalMetricValue
                        {
                            Arrivals = new List<int> {1}
                        }
                    },
                    new MetricPair
                    {
                        Key = new ArrivalMetricKey
                        {
                            RouteShortName = "2",
                            AgencyName = "Metro",
                            Headsign = "To Mall"
                        },
                        Value = new ArrivalMetricValue
                        {
                            Arrivals = new List<int> {2}
                        }
                    },
                    new MetricPair
                    {
                        Key = new ArrivalMetricKey
                        {
                            RouteShortName = "3",
                            AgencyName = "LADOT",
                            Headsign = "To Walmart"
                        },
                        Value = new ArrivalMetricValue
                        {
                            Arrivals = new List<int> {3}
                        }
                    },
                    new MetricPair
                    {
                        Key = new ArrivalMetricKey
                        {
                            RouteShortName = "1234",
                            AgencyName = "SPORTS",
                            Headsign = "To Target"
                        },
                        Value = new ArrivalMetricValue
                        {
                            Arrivals = new List<int> {4}
                        }
                    }
                },
                Messages = new List<SignMessage>
                {
                    new SignMessage
                    {
                        Id = 12,
                        Message = new Message
                        {
                            Text = "Test Message."
                        }
                    }
                }
            };

            var config = new SignConfiguration
            {
                SignType = signType
            };
            var protocol = CreateTestProbe();
            _actor.Tell((config, (IActorRef)protocol, state));

            protocol.ExpectMsg<ClearSign>();
            _actor.Tell(new Ack());

            protocol.ExpectMsg<SetRequest>(); // brightness
            _actor.Tell(new Ack());

            protocol.ExpectMsg<DownloadRequest>(request =>
            {
                var program = Encoding.ASCII.GetString(request.BodyData);
                program
                    .Should()
                    .Be(expectedProgram);
            });

            _ttsActor.ExpectMsg<IActorRef>().Should().Be(protocol);
            _ttsActor.ExpectMsg<SignState>().Should().Be(state);
        }

        [Fact]
        public void Should_compile_program_and_wait_for_syncs_for_one_metric_line()
        {
            var state = new SignState
            {
                Metrics = new List<MetricPair>
                {
                    new MetricPair
                    {
                        Key = new ArrivalMetricKey
                        {
                            RouteShortName = "TEST",
                            AgencyName = "METRO",
                            Headsign = "To Downtown"
                        },
                        Value = new ArrivalMetricValue
                        {
                            Arrivals = new List<int> {1}
                        }
                    }
                },
                Messages = new List<SignMessage>
                {
                    new SignMessage
                    {
                        Id = 12,
                        Message = new Message
                        {
                            Text = "Test Message."
                        }
                    }
                }
            };

            var config = new SignConfiguration
            {
                SignType = SignType.Sunrise
            };
            var protocol = CreateTestProbe();
            _actor.Tell((config, (IActorRef) protocol, state));

            protocol.ExpectMsg<ClearSign>();
            _actor.Tell(new Ack());

            protocol.ExpectMsg<SetRequest>(); // brightness
            _actor.Tell(new Ack());

            protocol.ExpectMsg<DownloadRequest>();
            _actor.Tell(new Ack());

            protocol.ExpectMsg<CompileAndGoRequest>();
            _actor.Tell(new Ack());

            _actor.Tell(new WaitForSync
            {
                ReceivedAt = TimeSpan.MaxValue
            });

            protocol.ExpectNoMsg(TimeSpan.FromMilliseconds(50));

            _actor.Tell(new WaitForSync
            {
                ReceivedAt = TimeSpan.MaxValue
            });

            protocol.ExpectNoMsg(TimeSpan.FromMilliseconds(50));

            _actor.Tell(new WaitForSync
            {
                ReceivedAt = TimeSpan.MaxValue
            });

            protocol.ExpectMsg<SyncRequest>();
        }

        [Fact]
        public void Should_compile_program_and_handle_timeout_sync_for_one_metric_line()
        {
            var state = new SignState
            {
                Metrics = new List<MetricPair>
                {
                    new MetricPair
                    {
                        Key = new ArrivalMetricKey
                        {
                            RouteShortName = "TEST"
                        },
                        Value = new ArrivalMetricValue
                        {
                            Arrivals = new List<int> {1}
                        }
                    }
                },
                Messages = new List<SignMessage>
                {
                    new SignMessage
                    {
                        Id = 12,
                        Message = new Message
                        {
                            Text = "Test Message."
                        }
                    }
                }
            };

            var config = new SignConfiguration
            {
                SignType = SignType.Sunrise
            };
            var protocol = CreateTestProbe();
            _actor.Tell((config, (IActorRef) protocol, state));

            protocol.ExpectMsg<ClearSign>();

            _actor.Tell(new Ack());

            protocol.ExpectMsg<SetRequest>(); // brightness
            _actor.Tell(new Ack());

            protocol.ExpectMsg<DownloadRequest>();

            _actor.Tell(new Ack());

            protocol.ExpectMsg<CompileAndGoRequest>();

            _actor.Tell(new Ack());

            protocol.ExpectNoMsg();

            Scheduler.Advance(TimeSpan.FromSeconds(30));

            protocol.ExpectMsg<SyncRequest>();
        }

        [Theory]
        [InlineData(SignType.Sunrise, 5)]
        [InlineData(SignType.SunriseCdp40, 11)]
        public void Should_compile_program_and_wait_for_syncs_for_two_metric_line(
            SignType signType,
            int syncRepeats)
        {
            var state = new SignState
            {
                Metrics = new List<MetricPair>
                {
                    new MetricPair
                    {
                        Key = new ArrivalMetricKey
                        {
                            RouteShortName = "TEST"
                        },
                        Value = new ArrivalMetricValue
                        {
                            Arrivals = new List<int> {1}
                        }
                    },
                    new MetricPair
                    {
                        Key = new ArrivalMetricKey
                        {
                            RouteShortName = "TEST2"
                        },
                        Value = new ArrivalMetricValue
                        {
                            Arrivals = new List<int> {2}
                        }
                    }
                },
                Messages = new List<SignMessage>
                {
                }
            };

            var config = new SignConfiguration
            {
                SignType = signType
            };
            var protocol = CreateTestProbe();
            _actor.Tell((config, (IActorRef) protocol, state));

            protocol.ExpectMsg<ClearSign>();
            _actor.Tell(new Ack());

            protocol.ExpectMsg<SetRequest>(); // brightness
            _actor.Tell(new Ack());

            protocol.ExpectMsg<DownloadRequest>();
            _actor.Tell(new Ack());

            protocol.ExpectMsg<CompileAndGoRequest>();
            _actor.Tell(new Ack());

            for (int i=0; i<syncRepeats; ++i)
            {
                _actor.Tell(new WaitForSync
            {
                ReceivedAt = TimeSpan.MaxValue
            });
            }

            protocol.ExpectNoMsg(TimeSpan.FromMilliseconds(50));

            _actor.Tell(new WaitForSync
            {
                ReceivedAt = TimeSpan.MaxValue
            });

            protocol.ExpectMsg<SyncRequest>();
        }

        [Fact]
        public void Should_stop_operator_when_requested_with_3_metrics_and_message_line()
        {
            var deathWatcher = CreateTestProbe();
            deathWatcher.Watch(_actor);

            var state = new SignState
            {
                Metrics = new List<MetricPair>
                {
                    new MetricPair
                    {
                        Key = new ArrivalMetricKey
                        {
                            RouteShortName = "TEST"
                        },
                        Value = new ArrivalMetricValue
                        {
                            Arrivals = new List<int> {1}
                        }
                    },
                    new MetricPair
                    {
                        Key = new ArrivalMetricKey
                        {
                            RouteShortName = "TEST"
                        },
                        Value = new ArrivalMetricValue
                        {
                            Arrivals = new List<int> {1}
                        }
                    },
                    new MetricPair
                    {
                        Key = new ArrivalMetricKey
                        {
                            RouteShortName = "TEST"
                        },
                        Value = new ArrivalMetricValue
                        {
                            Arrivals = new List<int> {1}
                        }
                    }
                },
                Messages = new List<SignMessage>
                {
                    new SignMessage
                    {
                        Id = 12,
                        Message = new Message
                        {
                            Text = "Test Message."
                        }
                    }
                }
            };

            var config = new SignConfiguration
            {
                SignType = SignType.Sunrise
            };
            var protocol = CreateTestProbe();
            _actor.Tell((config, (IActorRef) protocol, state));

            protocol.ExpectMsg<ClearSign>();
            _actor.Tell(new Ack());

            protocol.ExpectMsg<SetRequest>(); // brightness
            _actor.Tell(new Ack());

            protocol.ExpectMsg<DownloadRequest>();
            _actor.Tell(new Ack());

            protocol.ExpectMsg<CompileAndGoRequest>();
            _actor.Tell(new Ack());

            _actor.Tell(StopOperator.Instance);

            _actor.Tell(new WaitForSync
            {
                ReceivedAt = TimeSpan.MaxValue
            });
            _actor.Tell(new WaitForSync
            {
                ReceivedAt = TimeSpan.MaxValue
            });
            _actor.Tell(new WaitForSync
            {
                ReceivedAt = TimeSpan.MaxValue
            });
            protocol.ExpectMsg<SyncRequest>();
            _actor.Tell(new Ack());

            // update the variables for the just-shown page
            protocol.ExpectMsg<SetRequest>();
            _actor.Tell(new Ack());
            protocol.ExpectMsg<SetRequest>();
            _actor.Tell(new Ack());
            protocol.ExpectMsg<SetRequest>();
            _actor.Tell(new Ack());

            _actor.Tell(new WaitForSync
            {
                ReceivedAt = TimeSpan.MaxValue
            });
            _actor.Tell(new WaitForSync
            {
                ReceivedAt = TimeSpan.MaxValue
            });
            _actor.Tell(new WaitForSync
            {
                ReceivedAt = TimeSpan.MaxValue
            });
            protocol.ExpectMsg<SyncRequest>();
            _actor.Tell(new Ack());

            // update the variables for the just-shown page
            protocol.ExpectMsg<SetRequest>();
            _actor.Tell(new Ack());
            protocol.ExpectMsg<SetRequest>();
            _actor.Tell(new Ack());
            protocol.ExpectMsg<SetRequest>();
            _actor.Tell(new Ack());

            _actor.Tell(new WaitForSync
            {
                ReceivedAt = TimeSpan.MaxValue
            });
            _actor.Tell(new WaitForSync
            {
                ReceivedAt = TimeSpan.MaxValue
            });
            _actor.Tell(new WaitForSync
            {
                ReceivedAt = TimeSpan.MaxValue
            });
            ExpectMsg<OperatorStopped>();

            deathWatcher.ExpectMsg<Terminated>();
        }

        [Fact]
        public void Should_stop_operator_when_requested_with_3_metrics_and_no_message_line()
        {
            var deathWatcher = CreateTestProbe();
            deathWatcher.Watch(_actor);

            var state = new SignState
            {
                Metrics = new List<MetricPair>
                {
                    new MetricPair
                    {
                        Key = new ArrivalMetricKey
                        {
                            RouteShortName = "TEST"
                        },
                        Value = new ArrivalMetricValue
                        {
                            Arrivals = new List<int> {1}
                        }
                    },
                    new MetricPair
                    {
                        Key = new ArrivalMetricKey
                        {
                            RouteShortName = "TEST"
                        },
                        Value = new ArrivalMetricValue
                        {
                            Arrivals = new List<int> {1}
                        }
                    },
                    new MetricPair
                    {
                        Key = new ArrivalMetricKey
                        {
                            RouteShortName = "TEST"
                        },
                        Value = new ArrivalMetricValue
                        {
                            Arrivals = new List<int> {1}
                        }
                    }
                },
                Messages = new List<SignMessage>
                {
                }
            };

            var config = new SignConfiguration
            {
                SignType = SignType.Sunrise
            };
            var protocol = CreateTestProbe();
            _actor.Tell((config, (IActorRef) protocol, state));

            protocol.ExpectMsg<ClearSign>();
            _actor.Tell(new Ack());

            protocol.ExpectMsg<SetRequest>(); // brightness
            _actor.Tell(new Ack());

            protocol.ExpectMsg<DownloadRequest>();
            _actor.Tell(new Ack());

            protocol.ExpectMsg<CompileAndGoRequest>();
            _actor.Tell(new Ack());

            _actor.Tell(StopOperator.Instance);

            _actor.Tell(new WaitForSync{ ReceivedAt = TimeSpan.MaxValue });
            _actor.Tell(new WaitForSync{ ReceivedAt = TimeSpan.MaxValue });
            _actor.Tell(new WaitForSync{ ReceivedAt = TimeSpan.MaxValue });
            _actor.Tell(new WaitForSync{ ReceivedAt = TimeSpan.MaxValue });
            _actor.Tell(new WaitForSync{ ReceivedAt = TimeSpan.MaxValue });
            _actor.Tell(new WaitForSync{ ReceivedAt = TimeSpan.MaxValue });
            protocol.ExpectMsg<SyncRequest>();
            _actor.Tell(new Ack());

            // update the variables for the just-shown page
            for (int i=0; i<6; ++i)
            {
                protocol.ExpectMsg<SetRequest>();
                _actor.Tell(new Ack());
            }

            _actor.Tell(new WaitForSync{ ReceivedAt = TimeSpan.MaxValue });
            _actor.Tell(new WaitForSync{ ReceivedAt = TimeSpan.MaxValue });
            _actor.Tell(new WaitForSync{ ReceivedAt = TimeSpan.MaxValue });
            _actor.Tell(new WaitForSync{ ReceivedAt = TimeSpan.MaxValue });
            _actor.Tell(new WaitForSync{ ReceivedAt = TimeSpan.MaxValue });
            _actor.Tell(new WaitForSync{ ReceivedAt = TimeSpan.MaxValue });
            ExpectMsg<OperatorStopped>();

            deathWatcher.ExpectMsg<Terminated>();
        }

        [Theory]
        [InlineData(SignType.Sunrise)]
        [InlineData(SignType.SunriseCdp40)]
        [InlineData(SignType.SunriseSolar)]
        public void Should_set_brightness(SignType signType)
        {
            var state = new Fixture().Create<SignState>();
            var config = new SignConfiguration
            {
                SignType = signType
            };
            var protocol = CreateTestProbe();
            _actor.Tell((config, (IActorRef)protocol, state));

            protocol.ExpectMsg<ClearSign>();
            _actor.Tell(new Ack());

            // "0" means auto-brightness for non-solar signs
            var brightVal = signType == SignType.SunriseSolar ? "1" : "0";
            protocol.ExpectMsg<SetRequest>(request =>
            {
                Encoding.ASCII.GetString(request.BodyData)
                    .Should()
                    .Be($"var BRIGHT {brightVal}\r");
            });
            _actor.Tell(new Ack());

            protocol.ExpectMsg<DownloadRequest>();
        }

        [Fact]
        public void Should_update_tts_actor_when_receiving_new_metrics()
        {
        }

        [Theory]
        [InlineData(SignType.Sunrise, 2)]
        [InlineData(SignType.SunriseCdp40, 4)]
        public void Should_update_variable_values_for_more_than_two_program_pages(
            SignType signType,
            int metricsPerPage)
        {
            int pageCount = 5;
            int metricCount = metricsPerPage * pageCount;

            var state = new SignState
            {
                Metrics = new Fixture().CreateMany<MetricPair>(metricCount).ToList(),
                Messages = new List<SignMessage>()
            };

            var config = new SignConfiguration
            {
                SignType = signType
            };
            var protocol = CreateTestProbe();

            _actor.Tell((config, (IActorRef) protocol, state));

            protocol.ExpectMsg<ClearSign>();
            _actor.Tell(new Ack());

            protocol.ExpectMsg<SetRequest>(); // brightness
            _actor.Tell(new Ack());

            protocol.ExpectMsg<DownloadRequest>();
            _actor.Tell(new Ack());

            protocol.ExpectMsg<CompileAndGoRequest>();
            _actor.Tell(new Ack());

            // handle the first page on its own--
            // 3 sign areas times 1 page of metrics (metricCount/4)
            for (int i=0; i<3*metricsPerPage; ++i)
            {
                _actor.Tell(new WaitForSync { ReceivedAt = TimeSpan.MaxValue });
            }
            protocol.ExpectMsg<SyncRequest>();
            _actor.Tell(new Ack());

            // now loop over the remaining pages until everything has been shown 4 times--
            // 4 rounds through 5 pages (minus 1, the first page)
            for (int i=1; i<4*pageCount; ++i)
            {
                // first update the variables on the previous page
                for (int r=0; r<metricsPerPage; ++r)
                {
                    protocol.ExpectMsg<SetRequest>(); // short name
                    _actor.Tell(new Ack());
                    protocol.ExpectMsg<SetRequest>(); // headsign
                    _actor.Tell(new Ack());
                    protocol.ExpectMsg<SetRequest>(); // arrivals
                    _actor.Tell(new Ack());
                }

                // then wait for syncs from currently displayed page
                for (int a=0; a<3*metricsPerPage; ++a)
                {
                    _actor.Tell(new WaitForSync { ReceivedAt = TimeSpan.MaxValue });
                }
                protocol.ExpectMsg<SyncRequest>();
                _actor.Tell(new Ack());
            }

            // finally we clear the sign after 4 rounds through the content
            protocol.ExpectMsg<ClearSign>();
        }

        [Fact]
        public void Should_handle_timeout_on_variable_update()
        {
            // this is basically recreating the test that was for the metrics actor
        }

        [Fact]
        public void Should_fill_in_blanks_to_complete_a_page_when_updating_variables()
        {
            var metricsPerPage = 2; // for sunrise 2-line
            var metricCount = 5; // more than 2 pages, but an incomplete 3rd page

            var fixture = new Fixture();
            List<MetricPair> metrics = new List<MetricPair>();
            for (int i = 1; i <= metricCount; ++i)
            {
                var pair = fixture.Build<MetricPair>()
                    .With(p => p.Key, new ArrivalMetricKey
                    {
                        RouteShortName = $"{100 + i}",
                        Headsign = fixture.Create<string>()
                    })
                    .With(p => p.Value, new ArrivalMetricValue
                    {
                        Arrivals = new List<int> { i }
                    })
                    .Create();

                metrics.Add(pair);
            }

            var state = new SignState
            {
                Metrics = metrics,
                Messages = new List<SignMessage>()
            };

            var config = new SignConfiguration
            {
                SignType = SignType.Sunrise
            };
            var protocol = CreateTestProbe();

            _actor.Tell((config, (IActorRef) protocol, state));

            protocol.ExpectMsg<ClearSign>();
            _actor.Tell(new Ack());

            protocol.ExpectMsg<SetRequest>(); // brightness
            _actor.Tell(new Ack());

            protocol.ExpectMsg<DownloadRequest>();
            _actor.Tell(new Ack());

            protocol.ExpectMsg<CompileAndGoRequest>();
            _actor.Tell(new Ack());

            // handle the first page on its own--
            // 3 sign areas times 1 page of metrics
            for (int i=0; i<3*metricsPerPage; ++i)
            {
                _actor.Tell(new WaitForSync { ReceivedAt = TimeSpan.MaxValue });
            }
            protocol.ExpectMsg<SyncRequest>();
            _actor.Tell(new Ack());

            // now update the variables on the first page
            // with the values from the remaining metrics
            //   First short name
            protocol.ExpectMsg<SetRequest>(req =>
            {
                Encoding.ASCII
                    .GetString(req.BodyData)
                    .Should()
                    .Be("var str0 105\r");
            });
            _actor.Tell(new Ack());
            //   then headsign
            protocol.ExpectMsg<SetRequest>();
            _actor.Tell(new Ack());
            //   then arrivals
            protocol.ExpectMsg<SetRequest>(req =>
            {
                Encoding.ASCII
                    .GetString(req.BodyData)
                    .Should()
                    .Be("var str2 5 MIN\r");
            });
            _actor.Tell(new Ack());

            // since the above was the 5th and final metric,
            // but we still have one more row to fill on the sign,
            // the final row's areas should get blanks
            protocol.ExpectMsg<SetRequest>(req =>
            {
                Encoding.ASCII
                    .GetString(req.BodyData)
                    .Should()
                    .Be($"var str3  \r");
            });
            _actor.Tell(new Ack());
            protocol.ExpectMsg<SetRequest>(req =>
            {
                Encoding.ASCII
                    .GetString(req.BodyData)
                    .Should()
                    .Be($"var str4  \r");
            });
            _actor.Tell(new Ack());
            protocol.ExpectMsg<SetRequest>(req =>
            {
                Encoding.ASCII
                    .GetString(req.BodyData)
                    .Should()
                    .Be($"var str5  \r");
            });
            _actor.Tell(new Ack());

            // now sync the second page
            // 3 sign areas times 1 page of metrics
            for (int i=0; i<3*metricsPerPage; ++i)
            {
                _actor.Tell(new WaitForSync { ReceivedAt = TimeSpan.MaxValue });
            }
            protocol.ExpectMsg<SyncRequest>();
            _actor.Tell(new Ack());

            // now we're displaying the 3rd page (metric #5 plus the blank line)
            // and we've looped around so need to assign the first two metrics' values
            //   First short name
            protocol.ExpectMsg<SetRequest>(req =>
            {
                Encoding.ASCII
                    .GetString(req.BodyData)
                    .Should()
                    .Be("var str6 101\r");
            });
            _actor.Tell(new Ack());
            //   then headsign
            protocol.ExpectMsg<SetRequest>();
            _actor.Tell(new Ack());
            //   then arrivals
            protocol.ExpectMsg<SetRequest>(req =>
            {
                Encoding.ASCII
                    .GetString(req.BodyData)
                    .Should()
                    .Be("var str8 1 MIN\r");
            });
            _actor.Tell(new Ack());
        }
    }

    public class MetricsOnlyProgramGenerator : IEnumerable<object[]>
    {
        private readonly List<object[]> _data = new List<object[]>
        {
            new object[] { SignType.Sunrise, sunrise20Program },
            new object[] { SignType.SunriseCdp40, sunrise40Program },
            new object[] { SignType.SunriseCdp40x160, sunrise40x160Program },
        };

        public IEnumerator<object[]> GetEnumerator() => _data.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        private static readonly string sunrise20Program =
$@"progTESTset win A0 to 0 0 22 8
use font cd9
INITVAR str0 1
DISPLAYLEFT VAR str0
WAIT 3
WAITFORSYNC
INITVAR str6 3
DISPLAYLEFT VAR str6
WAIT 3
WAITFORSYNC
REPEAT
set win A1 to 24 0 102 8
use font cd9
INITVAR str1 METRO - TO 1 SOMEWHERE
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str1
WAITFORSYNC
INITVAR str7 COMMERCE - TO 3 SOMEWHERE
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str7
WAITFORSYNC
REPEAT
set win A2 to 103 0 143 8
use font cd9
INITVAR str2 1 MIN
DISPLAYRIGHT VAR str2
WAIT 3
WAITFORSYNC
INITVAR str8 3 MIN
DISPLAYRIGHT VAR str8
WAIT 3
WAITFORSYNC
REPEAT
set win A3 to 0 11 22 19
use font cd9
INITVAR str3 2
DISPLAYLEFT VAR str3
WAIT 3
WAITFORSYNC
INITVAR str9 1234
DISPLAYLEFT VAR str9
WAIT 3
WAITFORSYNC
REPEAT
set win A4 to 24 11 102 19
use font cd9
INITVAR str4 METRO - TO 2 SOMEWHERE
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str4
WAITFORSYNC
INITVAR str10 METRO - TO 4 SOMEWHERE
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str10
WAITFORSYNC
REPEAT
set win A5 to 103 11 143 19
use font cd9
INITVAR str5 2 MIN
DISPLAYRIGHT VAR str5
WAIT 3
WAITFORSYNC
INITVAR str11 4 MIN
DISPLAYRIGHT VAR str11
WAIT 3
WAITFORSYNC
REPEAT
"
            .Replace("\r\n", "\r")
            .Replace("\n", "\r");

        // NB: this is a single page of content but duplicated to work properly
        // with the multi-page logic
        private static readonly string sunrise40Program =
$@"progTESTset win A0 to 0 0 22 8
use font cd9
INITVAR str0 1
DISPLAYLEFT VAR str0
WAIT 3
WAITFORSYNC
INITVAR str12 1
DISPLAYLEFT VAR str12
WAIT 3
WAITFORSYNC
REPEAT
set win A1 to 24 0 159 8
use font cd9
INITVAR str1 METRO - TO 1 SOMEWHERE
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str1
WAITFORSYNC
INITVAR str13 METRO - TO 1 SOMEWHERE
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str13
WAITFORSYNC
REPEAT
set win A2 to 160 0 200 8
use font cd9
INITVAR str2 1 MIN
DISPLAYRIGHT VAR str2
WAIT 3
WAITFORSYNC
INITVAR str14 1 MIN
DISPLAYRIGHT VAR str14
WAIT 3
WAITFORSYNC
REPEAT
set win A3 to 0 10 22 18
use font cd9
INITVAR str3 2
DISPLAYLEFT VAR str3
WAIT 3
WAITFORSYNC
INITVAR str15 2
DISPLAYLEFT VAR str15
WAIT 3
WAITFORSYNC
REPEAT
set win A4 to 24 10 159 18
use font cd9
INITVAR str4 METRO - TO 2 SOMEWHERE
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str4
WAITFORSYNC
INITVAR str16 METRO - TO 2 SOMEWHERE
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str16
WAITFORSYNC
REPEAT
set win A5 to 160 10 200 18
use font cd9
INITVAR str5 2 MIN
DISPLAYRIGHT VAR str5
WAIT 3
WAITFORSYNC
INITVAR str17 2 MIN
DISPLAYRIGHT VAR str17
WAIT 3
WAITFORSYNC
REPEAT
set win A6 to 0 20 22 28
use font cd9
INITVAR str6 3
DISPLAYLEFT VAR str6
WAIT 3
WAITFORSYNC
INITVAR str18 3
DISPLAYLEFT VAR str18
WAIT 3
WAITFORSYNC
REPEAT
set win A7 to 24 20 159 28
use font cd9
INITVAR str7 COMMERCE - TO 3 SOMEWHERE
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str7
WAITFORSYNC
INITVAR str19 COMMERCE - TO 3 SOMEWHERE
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str19
WAITFORSYNC
REPEAT
set win A8 to 160 20 200 28
use font cd9
INITVAR str8 3 MIN
DISPLAYRIGHT VAR str8
WAIT 3
WAITFORSYNC
INITVAR str20 3 MIN
DISPLAYRIGHT VAR str20
WAIT 3
WAITFORSYNC
REPEAT
set win A9 to 0 30 22 38
use font cd9
INITVAR str9 1234
DISPLAYLEFT VAR str9
WAIT 3
WAITFORSYNC
INITVAR str21 1234
DISPLAYLEFT VAR str21
WAIT 3
WAITFORSYNC
REPEAT
set win A10 to 24 30 159 38
use font cd9
INITVAR str10 METRO - TO 4 SOMEWHERE
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str10
WAITFORSYNC
INITVAR str22 METRO - TO 4 SOMEWHERE
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str22
WAITFORSYNC
REPEAT
set win A11 to 160 30 200 38
use font cd9
INITVAR str11 4 MIN
DISPLAYRIGHT VAR str11
WAIT 3
WAITFORSYNC
INITVAR str23 4 MIN
DISPLAYRIGHT VAR str23
WAIT 3
WAITFORSYNC
REPEAT
"
            .Replace("\r\n", "\r")
            .Replace("\n", "\r");

        // NB: this is a single page of content but duplicated to work properly
        // with the multi-page logic
        private static readonly string sunrise40x160Program =
$@"progTESTset win A0 to 0 0 22 8
use font cd9
INITVAR str0 1
DISPLAYLEFT VAR str0
WAIT 3
WAITFORSYNC
INITVAR str12 1
DISPLAYLEFT VAR str12
WAIT 3
WAITFORSYNC
REPEAT
set win A1 to 24 0 119 8
use font cd9
INITVAR str1 METRO - TO 1 SOMEWHERE
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str1
WAITFORSYNC
INITVAR str13 METRO - TO 1 SOMEWHERE
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str13
WAITFORSYNC
REPEAT
set win A2 to 120 0 160 8
use font cd9
INITVAR str2 1 MIN
DISPLAYRIGHT VAR str2
WAIT 3
WAITFORSYNC
INITVAR str14 1 MIN
DISPLAYRIGHT VAR str14
WAIT 3
WAITFORSYNC
REPEAT
set win A3 to 0 10 22 18
use font cd9
INITVAR str3 2
DISPLAYLEFT VAR str3
WAIT 3
WAITFORSYNC
INITVAR str15 2
DISPLAYLEFT VAR str15
WAIT 3
WAITFORSYNC
REPEAT
set win A4 to 24 10 119 18
use font cd9
INITVAR str4 METRO - TO 2 SOMEWHERE
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str4
WAITFORSYNC
INITVAR str16 METRO - TO 2 SOMEWHERE
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str16
WAITFORSYNC
REPEAT
set win A5 to 120 10 160 18
use font cd9
INITVAR str5 2 MIN
DISPLAYRIGHT VAR str5
WAIT 3
WAITFORSYNC
INITVAR str17 2 MIN
DISPLAYRIGHT VAR str17
WAIT 3
WAITFORSYNC
REPEAT
set win A6 to 0 20 22 28
use font cd9
INITVAR str6 3
DISPLAYLEFT VAR str6
WAIT 3
WAITFORSYNC
INITVAR str18 3
DISPLAYLEFT VAR str18
WAIT 3
WAITFORSYNC
REPEAT
set win A7 to 24 20 119 28
use font cd9
INITVAR str7 COMMERCE - TO 3 SOMEWHERE
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str7
WAITFORSYNC
INITVAR str19 COMMERCE - TO 3 SOMEWHERE
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str19
WAITFORSYNC
REPEAT
set win A8 to 120 20 160 28
use font cd9
INITVAR str8 3 MIN
DISPLAYRIGHT VAR str8
WAIT 3
WAITFORSYNC
INITVAR str20 3 MIN
DISPLAYRIGHT VAR str20
WAIT 3
WAITFORSYNC
REPEAT
set win A9 to 0 30 22 38
use font cd9
INITVAR str9 1234
DISPLAYLEFT VAR str9
WAIT 3
WAITFORSYNC
INITVAR str21 1234
DISPLAYLEFT VAR str21
WAIT 3
WAITFORSYNC
REPEAT
set win A10 to 24 30 119 38
use font cd9
INITVAR str10 METRO - TO 4 SOMEWHERE
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str10
WAITFORSYNC
INITVAR str22 METRO - TO 4 SOMEWHERE
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str22
WAITFORSYNC
REPEAT
set win A11 to 120 30 160 38
use font cd9
INITVAR str11 4 MIN
DISPLAYRIGHT VAR str11
WAIT 3
WAITFORSYNC
INITVAR str23 4 MIN
DISPLAYRIGHT VAR str23
WAIT 3
WAITFORSYNC
REPEAT
"
            .Replace("\r\n", "\r")
            .Replace("\n", "\r");
   }

    public class MetricsAndMessageProgramGenerator : IEnumerable<object[]>
    {
        private readonly List<object[]> _data = new List<object[]>
        {
            new object[] { SignType.Sunrise, sunrise20Program },
            new object[] { SignType.SunriseCdp40, sunrise40Program },
            new object[] { SignType.SunriseCdp40x160, sunrise40x160Program },
        };

        public IEnumerator<object[]> GetEnumerator() => _data.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        private static readonly string sunrise20Program =
$@"progTESTset win A0 to 0 0 22 8
use font cd9
INITVAR str0 1
DISPLAYLEFT VAR str0
WAIT 3
WAITFORSYNC
INITVAR str3 2
DISPLAYLEFT VAR str3
WAIT 3
WAITFORSYNC
REPEAT
set win A1 to 24 0 102 8
use font cd9
INITVAR str1 COMMERCE - TO DOWNTOWN
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str1
WAITFORSYNC
INITVAR str4 METRO - TO MALL
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str4
WAITFORSYNC
REPEAT
set win A2 to 103 0 143 8
use font cd9
INITVAR str2 1 MIN
DISPLAYRIGHT VAR str2
WAIT 3
WAITFORSYNC
INITVAR str5 2 MIN
DISPLAYRIGHT VAR str5
WAIT 3
WAITFORSYNC
REPEAT
set win A3 to 0 11 143 20
use font cd9
PACE 3
STREAM STRING TEST MESSAGE.{" "}
REPEAT
"
                        .Replace("\r\n", "\r")
                        .Replace("\n", "\r");

        // NB: this is a single page of content but duplicated to work properly
        // with the multi-page logic
        private static readonly string sunrise40Program =
$@"progTESTset win A0 to 0 0 22 8
use font cd9
INITVAR str0 1
DISPLAYLEFT VAR str0
WAIT 3
WAITFORSYNC
INITVAR str9 1
DISPLAYLEFT VAR str9
WAIT 3
WAITFORSYNC
REPEAT
set win A1 to 24 0 159 8
use font cd9
INITVAR str1 COMMERCE - TO DOWNTOWN
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str1
WAITFORSYNC
INITVAR str10 COMMERCE - TO DOWNTOWN
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str10
WAITFORSYNC
REPEAT
set win A2 to 160 0 200 8
use font cd9
INITVAR str2 1 MIN
DISPLAYRIGHT VAR str2
WAIT 3
WAITFORSYNC
INITVAR str11 1 MIN
DISPLAYRIGHT VAR str11
WAIT 3
WAITFORSYNC
REPEAT
set win A3 to 0 10 22 18
use font cd9
INITVAR str3 2
DISPLAYLEFT VAR str3
WAIT 3
WAITFORSYNC
INITVAR str12 2
DISPLAYLEFT VAR str12
WAIT 3
WAITFORSYNC
REPEAT
set win A4 to 24 10 159 18
use font cd9
INITVAR str4 METRO - TO MALL
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str4
WAITFORSYNC
INITVAR str13 METRO - TO MALL
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str13
WAITFORSYNC
REPEAT
set win A5 to 160 10 200 18
use font cd9
INITVAR str5 2 MIN
DISPLAYRIGHT VAR str5
WAIT 3
WAITFORSYNC
INITVAR str14 2 MIN
DISPLAYRIGHT VAR str14
WAIT 3
WAITFORSYNC
REPEAT
set win A6 to 0 20 22 28
use font cd9
INITVAR str6 {" "}
DISPLAYLEFT VAR str6
WAIT 3
WAITFORSYNC
INITVAR str15 {" "}
DISPLAYLEFT VAR str15
WAIT 3
WAITFORSYNC
REPEAT
set win A7 to 24 20 159 28
use font cd9
INITVAR str7 {" "}
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str7
WAITFORSYNC
INITVAR str16 {" "}
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str16
WAITFORSYNC
REPEAT
set win A8 to 160 20 200 28
use font cd9
INITVAR str8 {" "}
DISPLAYRIGHT VAR str8
WAIT 3
WAITFORSYNC
INITVAR str17 {" "}
DISPLAYRIGHT VAR str17
WAIT 3
WAITFORSYNC
REPEAT
set win A9 to 0 31 200 40
use font cd9
PACE 3
STREAM STRING TEST MESSAGE.{" "}
REPEAT
"
            .Replace("\r\n", "\r")
            .Replace("\n", "\r");

        // NB: this is a single page of content but duplicated to work properly
        // with the multi-page logic
        private static readonly string sunrise40x160Program =
$@"progTESTset win A0 to 0 0 22 8
use font cd9
INITVAR str0 1
DISPLAYLEFT VAR str0
WAIT 3
WAITFORSYNC
INITVAR str9 1
DISPLAYLEFT VAR str9
WAIT 3
WAITFORSYNC
REPEAT
set win A1 to 24 0 119 8
use font cd9
INITVAR str1 COMMERCE - TO DOWNTOWN
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str1
WAITFORSYNC
INITVAR str10 COMMERCE - TO DOWNTOWN
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str10
WAITFORSYNC
REPEAT
set win A2 to 120 0 160 8
use font cd9
INITVAR str2 1 MIN
DISPLAYRIGHT VAR str2
WAIT 3
WAITFORSYNC
INITVAR str11 1 MIN
DISPLAYRIGHT VAR str11
WAIT 3
WAITFORSYNC
REPEAT
set win A3 to 0 10 22 18
use font cd9
INITVAR str3 2
DISPLAYLEFT VAR str3
WAIT 3
WAITFORSYNC
INITVAR str12 2
DISPLAYLEFT VAR str12
WAIT 3
WAITFORSYNC
REPEAT
set win A4 to 24 10 119 18
use font cd9
INITVAR str4 METRO - TO MALL
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str4
WAITFORSYNC
INITVAR str13 METRO - TO MALL
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str13
WAITFORSYNC
REPEAT
set win A5 to 120 10 160 18
use font cd9
INITVAR str5 2 MIN
DISPLAYRIGHT VAR str5
WAIT 3
WAITFORSYNC
INITVAR str14 2 MIN
DISPLAYRIGHT VAR str14
WAIT 3
WAITFORSYNC
REPEAT
set win A6 to 0 20 22 28
use font cd9
INITVAR str6 {" "}
DISPLAYLEFT VAR str6
WAIT 3
WAITFORSYNC
INITVAR str15 {" "}
DISPLAYLEFT VAR str15
WAIT 3
WAITFORSYNC
REPEAT
set win A7 to 24 20 119 28
use font cd9
INITVAR str7 {" "}
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str7
WAITFORSYNC
INITVAR str16 {" "}
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str16
WAITFORSYNC
REPEAT
set win A8 to 120 20 160 28
use font cd9
INITVAR str8 {" "}
DISPLAYRIGHT VAR str8
WAIT 3
WAITFORSYNC
INITVAR str17 {" "}
DISPLAYRIGHT VAR str17
WAIT 3
WAITFORSYNC
REPEAT
set win A9 to 0 31 160 40
use font cd9
PACE 3
STREAM STRING TEST MESSAGE.{" "}
REPEAT
"
            .Replace("\r\n", "\r")
            .Replace("\n", "\r");
    }

    public class LongerMetricsAndMessageProgramGenerator : IEnumerable<object[]>
    {
        private readonly List<object[]> _data = new List<object[]>
        {
            new object[] { SignType.SunriseCdp40, sunrise40Program },
        };

        public IEnumerator<object[]> GetEnumerator() => _data.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        private static readonly string sunrise40Program =
$@"progTESTset win A0 to 0 0 22 8
use font cd9
INITVAR str0 1
DISPLAYLEFT VAR str0
WAIT 3
WAITFORSYNC
INITVAR str9 1234
DISPLAYLEFT VAR str9
WAIT 3
WAITFORSYNC
REPEAT
set win A1 to 24 0 159 8
use font cd9
INITVAR str1 COMMERCE - TO DOWNTOWN
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str1
WAITFORSYNC
INITVAR str10 SPORTS - TO TARGET
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str10
WAITFORSYNC
REPEAT
set win A2 to 160 0 200 8
use font cd9
INITVAR str2 1 MIN
DISPLAYRIGHT VAR str2
WAIT 3
WAITFORSYNC
INITVAR str11 4 MIN
DISPLAYRIGHT VAR str11
WAIT 3
WAITFORSYNC
REPEAT
set win A3 to 0 10 22 18
use font cd9
INITVAR str3 2
DISPLAYLEFT VAR str3
WAIT 3
WAITFORSYNC
INITVAR str12 {" "}
DISPLAYLEFT VAR str12
WAIT 3
WAITFORSYNC
REPEAT
set win A4 to 24 10 159 18
use font cd9
INITVAR str4 METRO - TO MALL
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str4
WAITFORSYNC
INITVAR str13 {" "}
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str13
WAITFORSYNC
REPEAT
set win A5 to 160 10 200 18
use font cd9
INITVAR str5 2 MIN
DISPLAYRIGHT VAR str5
WAIT 3
WAITFORSYNC
INITVAR str14 {" "}
DISPLAYRIGHT VAR str14
WAIT 3
WAITFORSYNC
REPEAT
set win A6 to 0 20 22 28
use font cd9
INITVAR str6 3
DISPLAYLEFT VAR str6
WAIT 3
WAITFORSYNC
INITVAR str15 {" "}
DISPLAYLEFT VAR str15
WAIT 3
WAITFORSYNC
REPEAT
set win A7 to 24 20 159 28
use font cd9
INITVAR str7 LADOT - TO WALMART
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str7
WAITFORSYNC
INITVAR str16 {" "}
PACE 3
DISPLAYLEFT string {" "}
STREAM VAR str16
WAITFORSYNC
REPEAT
set win A8 to 160 20 200 28
use font cd9
INITVAR str8 3 MIN
DISPLAYRIGHT VAR str8
WAIT 3
WAITFORSYNC
INITVAR str17 {" "}
DISPLAYRIGHT VAR str17
WAIT 3
WAITFORSYNC
REPEAT
set win A9 to 0 31 200 40
use font cd9
PACE 3
STREAM STRING TEST MESSAGE.{" "}
REPEAT
"
            .Replace("\r\n", "\r")
            .Replace("\n", "\r");
    }
}
